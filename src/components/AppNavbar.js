import {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import {Navbar, Container, Nav} from 'react-bootstrap';
import UserContext from '../UserContext'

export default function AppNavbar() {

  const {user} = useContext(UserContext); // with provider

  // state hook to store the user information stored in the login page (if without provider)
  // const [user, setUser] = useState(localStorage.getItem('email'));
  console.log(user)

	return (
	<Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to='/'>Batch 196 Booking App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to='/'>Home</Nav.Link>
            <Nav.Link as={Link} to='/courses'>Courses</Nav.Link>
            {
              (user.id !== null) ?
              <Nav.Link as={Link} to='/logout'>Logout</Nav.Link>
              :
              <>
                <Nav.Link as={Link} to='/login'>Login</Nav.Link>
                <Nav.Link as={Link} to='/register'>Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

		)
};

