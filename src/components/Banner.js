import {Row, Col, Button} from 'react-bootstrap';

export default function Banner(){
	return(
		<Row>
			<Col className='p-5'>
				<h1>B196 Booking App</h1>
				<p>Opportunities for everyone, everywhere</p>
				<Button variant='primary'>Enroll Now</Button>
			</Col>
		</Row>

	)
}