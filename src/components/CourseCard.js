import {useState} from 'react';
import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {
	// console.log(props);	// result: PHP-Laravel (coursesData[0])
						// every component receives information in a form of an object
	// console.log(typeof props); // result: Object

	// object destructuring
	const {name, description, price, _id} = courseProp;

	/*
		useState is a react hooks. It store its state.
		Syntax:
			const [getter, setter] = useState(initialGetterValue);
			getter - will get changes
			setter - will make the changes
	*/

	// const [slot, setSlot] = useState(10);
	// const [count, setCount] = useState(0);
	// 	const enroll = () => {
	// 		if (slot === 0) {
	// 			alert("Sorry. No more seats available. Please try again later.")
	// 		} else {
	// 			(setSlot(slot - 1));
	// 			(setCount(count +1));
	// 		};
	// 	};
/*
	Alternative Solution:
	
	fuction enroll(){
		if (seats > 0){
			setCount(count + 1);
			console.log
		}
	};
*/	
	return(
		<Card className='mt-3 mb-3'>
			<Card.Body>
				<Card.Title>
					<h4>{name}</h4>
				</Card.Title>
				<Card.Subtitle>Course Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Course Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
			</Card.Body>
		</Card>
	)
}