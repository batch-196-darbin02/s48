import {useState, useEffect} from 'react';
import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
export default function	Courses(){
	// console.log(coursesData); // to check if the mock data is captured
	// console.log(coursesData[0]);

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp = {course}/>
				)
			}))
		})

	}, [])

	 // map creates a new array
	// const courses = coursesData.map(course => {
	// 	return(
	// 		<CourseCard key={course.id} courseProp = {course}/>
	// 	)
	// });

	return(
		<>
			<h1>Available Courses:</h1>
			{courses}
		</>
	);
}
// courseProp is like a variable. Can be named differently.
// Curly braces signifies that it is a JS logic

// process of passing through the props
// from the Courses Page > CoursesCard = PHP-Laravel
// from CourseCard > props === parameter