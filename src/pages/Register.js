import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user, setUser} = useContext(UserContext);
	const history = useNavigate();

	// state hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// check if values are successfully binded:
	// console.log(email);
	// console.log(password);

	function registerUser(e){
		// prevents page redirection via form submission
		e.preventDefault();

		fetch(`http://localhost:4000/users/checkEmailExists`,{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) // result: boolean

			if(data){
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'info',
					text: `The email that you're trying to register already exist.`
				})
			} else {
				fetch('http://localhost:4000/users',{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data.email){
						Swal.fire({
							title: 'Registration successful!',
							icon: 'success',
							text: 'Thank you for registering'
						});
						history('/login');
					} else {
						Swal.fire({
							title: 'Registration failed.',
							icon: 'error',
							text: 'Something went wrong, try again'
						})
					}
				})
			}
		})

		// localStorage.setItem('email', email);

		// setUser({
		// 	email: localStorage.getItem('email')
		// });

		// clear input fields
		setEmail('');
		setPassword('')
		setFirstName('');
		setLastName('');
		setMobileNo('');
	}

	// create validation to enable the submit button when all fields are populated and both passwords matched
	// syntax: useEffect (() => {}, [])
	// [] changes
	useEffect (() => {
		if(email !== '' && firstName !== '' && lastName !== '' && mobileNo !== '' && mobileNo.length === 11 && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		};

	}, [email,firstName,lastName,mobileNo,password])
	return(
		(user.id !== null) ?
			<Navigate to='/courses'/>
		:
		<>
		<h1>Register Here</h1>
		<Form onSubmit={e => registerUser(e)}>
			<Form.Group controlId='firstName'>
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type='text' placeholder='Enter your first name here' required value={firstName}
					onChange={e => setFirstName(e.target.value)} />
			</Form.Group>

			<Form.Group controlId='lastName'>
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type='text' placeholder='Enter your last name here' required value={lastName}
					onChange={e => setLastName(e.target.value)} />
			</Form.Group>

			<Form.Group controlId='mobileNo'>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type='number' placeholder='Enter your 11-digit mobile number here' required value={mobileNo}
					onChange={e => setMobileNo(e.target.value)} />
			</Form.Group>

			<Form.Group controlId='userEmail'>
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type='email' placeholder='Enter your email here' required value={email}
					onChange={e => setEmail(e.target.value)} />
				<Form.Text className='text-muted'>
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId='password'>
				<Form.Label>Password</Form.Label>
				<Form.Control type='password' placeholder='Enter your password here' required 
				value={password} onChange={e => setPassword(e.target.value)} />
			</Form.Group>


			{ isActive ?
				<Button className="mt-3 mb-5" variant='success' type='submit' id='submitBtn'>
				Register
				</Button>
				:
				<Button className="mt-3 mb-5" variant='danger' type='submit' id='submitBtn' disabled>
				Register
				</Button>
			}
		</Form>
		</>
	)
}

// in HTML:
// review s05 regarding the connection of <label> and <input>

// required - can't proceed if email address is password were not input

/*
	if(isActive === true){
		button is success
	} else {
		button is danger
	}
*/