import {Link} from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';

export default function Page(){
	return(
		<Row>
			<Col className='p-5'>
				<h1>404 Error</h1>
				<h1>Page Not Found</h1>
				<Button className="mt-3 mb-5" variant='primary' type='submit' as={Link} to='/'>
				Back Home
				</Button>
			</Col>
		</Row>

	)
}