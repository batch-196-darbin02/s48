import React from 'react';


// Creates a Context Object
// Context Object - data type of an object that can be used to store information that can be shared to other components within the app
const UserContext = React.createContext();

// Provider component allows other components to consume/use the context object and the supply necessary information needed
export const UserProvider = UserContext.Provider;

export default UserContext;




// UserContext = {
// 	user: '',
// 	setUser: function (),
// 	unsetUser: function ()
// }