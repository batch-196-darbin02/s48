import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // StrictMode highlights error that we may encounter
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Tony Stark";
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: 'Levi',
//   lastName: 'Ackerman'
// };

// const formatName = (user) => {
//   return user.firstName + ' ' + user.lastName;
// };

// const fullName = <h1>Hello, {formatName(user)}</h1>

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(fullName);


/*
  Before React18:

  ReactDOM.render(
    element,
    document.getElementById('root')
  );
*/