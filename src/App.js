import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
import Courses from './pages/Courses';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

	// State hook for the user state defined here for global scope
	// Initialized as an object with properties from the localStorage
	// This will be used to store the user information and will be used for validating if a user is logged in on the app ot not
	const [user, setUser] = useState({
		// email: localStorage.getItem('email') // replaced below since we already have token
		id: null,
		isAdmin: null
	});

	const unsetUser = () => {
		localStorage.clear();
	};

	useEffect (() => {
		fetch('http://localhost:4000/users/getUserDetails',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// Captured the data of whoever is logged in
			console.log(data);

			if(typeof data._id !== 'undefined'){
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});

			} else {

				// Set back the initial state of user
				setUser({
					id: null,
					isAdmin: null
				});
			}
		});
	}, []);

	return (
		<>
			<UserProvider value={{user, setUser, unsetUser}}>
				<Router>
					<AppNavbar/>
					<Container>
						<Routes>
							<Route exact path='/' element={<Home/>} />
							<Route exact path='/courses' element={<Courses/>} />
							<Route exact path='/courseView/:courseId' element={<CourseView/>} />
							<Route exact path='/register' element={<Register/>} />
							<Route exact path='/login' element={<Login/>} />
							<Route exact path='/logout' element={<Logout/>} />
							<Route exact path='*' element={<Error/>} />

						</Routes>
					</Container>
				</Router>
			</UserProvider>
		</>
	)

}

export default App;

// <Router></Router> enables the routing for page components
// <Routes></Routes> groups or specifies the pages that will have route
// <Route/> tells the path